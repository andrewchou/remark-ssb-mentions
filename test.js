const test = require('tape');
const mentions = require('.');

test('mentions in links are detected', function (t) {
  t.deepEquals(
    mentions('[@feed](@3HO6R2i60XNR3h6XCHAWCdt1k9Dwy+gaa2rVs6LzZ6Y=.ed25519)'),
    [
      {
        link: '@3HO6R2i60XNR3h6XCHAWCdt1k9Dwy+gaa2rVs6LzZ6Y=.ed25519',
        name: 'feed',
      },
    ],
    'feed link',
  );

  t.deepEquals(
    mentions(
      '[@feed](ssb:feed/ed25519/-oaWWDs8g73EZFUMfW37R_ULtFEjwKN_DczvdYihjbU=)',
    ),
    [
      {
        link: '@+oaWWDs8g73EZFUMfW37R/ULtFEjwKN/DczvdYihjbU=.ed25519',
        name: 'feed',
      },
    ],
    'SSB URI feed link',
  );

  t.deepEquals(
    mentions('[a msg](%A2LvseOYKDXyuSGlXl3Sz0F5j2khVCN6JTf8ORD/tM8=.sha256)'),
    [
      {
        link: '%A2LvseOYKDXyuSGlXl3Sz0F5j2khVCN6JTf8ORD/tM8=.sha256',
        name: 'a msg',
      },
    ],
    'msg link',
  );

  t.deepEquals(
    mentions(
      '[my msg](ssb:message/sha256/g3hPVPDEO1Aj_uPl0-J2NlhFB2bbFLIHlty-YuqFZ3w=)',
    ),
    [
      {
        link: '%g3hPVPDEO1Aj/uPl0+J2NlhFB2bbFLIHlty+YuqFZ3w=.sha256',
        name: 'my msg',
      },
    ],
    'SSB URI msg link',
  );

  t.deepEquals(
    mentions(
      '[a secret msg](%A2LvseOYKDXyuSGlXl3Sz0F5j2khVCN6JTf8ORD/tM8=.sha256?unbox=9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7=.boxs)',
    ),
    [
      {
        link: '%A2LvseOYKDXyuSGlXl3Sz0F5j2khVCN6JTf8ORD/tM8=.sha256',
        name: 'a secret msg',
        query: {
          unbox: '9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7=.boxs',
        },
      },
    ],
    'msg link with unbox',
  );

  t.deepEquals(
    mentions('[a blob](&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256)'),
    [
      {
        link: '&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256',
        name: 'a blob',
      },
    ],
    'blob link',
  );

  t.deepEquals(
    mentions(
      '![an image blob](&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256)',
    ),
    [
      {
        link: '&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256',
        name: 'an image blob',
      },
    ],
    'blob image',
  );

  t.deepEquals(
    mentions(
      '[a blob](&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256?unbox=A2LvseOYKDXyuSGlXl3Sz0F5j2khVCN6JTf8ORD/tM8=.boxs)',
    ),
    [
      {
        link: '&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256',
        name: 'a blob',
        query: {
          unbox: 'A2LvseOYKDXyuSGlXl3Sz0F5j2khVCN6JTf8ORD/tM8=.boxs',
        },
      },
    ],
    'secret blob link',
  );

  t.end();
});

test('does not detect URLs as mentions', function (t) {
  t.deepEquals(
    mentions(
      `Here is a link to my favorite repo: ` +
        `https://github.com/ssbc/ssb-db\n\n` +
        `You should check it out.`,
    ),
    [],
    'web URL',
  );

  t.deepEquals(
    mentions(
      `Here is a link to my favorite repo: ` +
        `https://github.com/ssbc/ssb-db/blob/6c0cdd580b6166b106283e3f1745585e189252ce/create.js#L46\n\n` +
        `You should check it out.`,
    ),
    [],
    'web URL with # in the middle',
  );

  t.end();
});

test('ref mentions are detected', function (t) {
  t.deepEquals(
    mentions('@3HO6R2i60XNR3h6XCHAWCdt1k9Dwy+gaa2rVs6LzZ6Y=.ed25519'),
    [
      {
        link: '@3HO6R2i60XNR3h6XCHAWCdt1k9Dwy+gaa2rVs6LzZ6Y=.ed25519',
        name: undefined,
      },
    ],
    'feed link',
  );

  t.deepEquals(
    mentions('%A2LvseOYKDXyuSGlXl3Sz0F5j2khVCN6JTf8ORD/tM8=.sha256'),
    [
      {
        link: '%A2LvseOYKDXyuSGlXl3Sz0F5j2khVCN6JTf8ORD/tM8=.sha256',
        name: undefined,
      },
    ],
    'msg link',
  );

  t.deepEquals(
    mentions('&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256'),
    [
      {
        link: '&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256',
        name: undefined,
      },
    ],
    'blob link',
  );

  t.end();
});

test('bare feed name mentions can be detected', function (t) {
  t.deepEquals(
    mentions('a @feed mention', {bareFeedNames: true}),
    [{name: 'feed', link: '@'}],
    'feed link',
  );
  t.end();
});

test('detect hashtags', function (t) {
  t.plan(1);
  t.deepEquals(
    mentions('a nice #hashtag here'),
    [{link: '#hashtag'}],
    'hashtag link',
  );
});

test('detect unicode hashtags', function (t) {
  t.plan(1);
  t.deepEquals(
    mentions('a nice #āhau here'),
    [{link: '#āhau'}],
    'hashtag link',
  );
});

test('detect hashtags with numbers', function (t) {
  t.plan(1);
  t.deepEquals(mentions('a nice #p2p here'), [{link: '#p2p'}], 'hashtag link');
});

test('detect hashtags with hyphens', function (t) {
  t.plan(1);
  t.deepEquals(
    mentions('i am a #new-people post'),
    [{link: '#new-people'}],
    'hashtag link with hyphens',
  );
});

test('no html tags in link names', function (t) {
  t.deepEquals(
    mentions(
      'link: [`code` *em* **strong** ~~del~~]' +
        '(&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256)',
    ),
    [
      {
        link: '&9SSTQys34p9f4zqjxvRwENjFX0JapgtesRey7+fxK14=.sha256',
        name: 'code em strong del',
      },
    ],
    'no tags',
  );
  t.end();
});

test.skip('detect emoji', function (t) {
  t.deepEquals(
    mentions('some nice :+1: :emoji: here', {emoji: true}),
    [
      {name: '+1', emoji: true},
      {name: 'emoji', emoji: true},
    ],
    'emoji',
  );
  t.end();
});
