const Ref = require('ssb-ref');
const SSBURI = require('ssb-uri2');
const unified = require('unified');
const remarkParse = require('remark-parse');
const selectAll = require('unist-util-select').selectAll;
const nodeToString = require('mdast-util-to-string');
const linkifyRegex = require('remark-linkify-regex');
const getUnicodeWordRegex = require('unicode-word-regex');

const bareFeedRegex = () => /\@\w+/g;
const bareFeedOnlyRegex = () => /^\@\w+$/g;
const hashtagRegex = () => {
  return new RegExp('#(' + getUnicodeWordRegex().source + '|\\d|-)+', 'gu');
};
const hashtagOnlyRegex = () => {
  return new RegExp('^#(' + getUnicodeWordRegex().source + '|\\d|-)+$', 'gu');
};

function linkIsMention(opts) {
  return function (node) {
    let name = nodeToString(node);
    if (SSBURI.isMessageSSBURI(node.url)) return true;
    if (SSBURI.isFeedSSBURI(node.url)) return true;
    if (Ref.isLink(node.url)) return true;
    if (opts.bareFeedNames && bareFeedOnlyRegex().test(name)) return true;
    if (hashtagOnlyRegex().test(name)) return true;
    return false;
  };
}

function extractMentions(opts) {
  function extractMentionsFromNode(node) {
    let link = node.url;
    let name = nodeToString(node);
    let result = {link, name};
    // postprocess link
    let unbox;
    if (link.includes('?unbox=')) {
      [link, unbox] = link.split('?unbox=');
      result.link = link;
      result.query = {unbox};
    }
    if (opts.bareFeedNames && name === link) {
      result.link = link = '@';
    }
    // postprocess name
    if (Ref.isLink(name)) {
      result.name = name = undefined;
    }
    if (SSBURI.isMessageSSBURI(link)) {
      result.link = SSBURI.toMessageSigil(link);
    }
    if (SSBURI.isFeedSSBURI(link)) {
      result.link = SSBURI.toFeedSigil(link);
    }
    if (name && name.startsWith('@') && !name.includes(' ')) {
      result.name = name = name.substr(1);
    }
    if (link === name && link.startsWith('#')) {
      name = undefined;
      delete result.name;
    }
    return result;
  }

  return (ast) =>
    selectAll('link', ast)
      .concat(selectAll('image', ast))
      .filter(linkIsMention(opts))
      .map(extractMentionsFromNode);
}

module.exports = function mentions(text, opts) {
  const config = opts || {};

  const mdast = unified().use(remarkParse).parse(text);

  const metadata = unified()
    .use(linkifyRegex(Ref.feedIdRegex))
    .use(linkifyRegex(Ref.msgIdRegex))
    .use(linkifyRegex(Ref.blobIdRegex))
    .use(linkifyRegex(SSBURI.getFeedSSBURIRegex()))
    .use(linkifyRegex(hashtagRegex()))
    .use(config.bareFeedNames && linkifyRegex(bareFeedRegex()))
    .use(extractMentions, config)
    .runSync(mdast);

  return metadata;
};
