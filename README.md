# remark-ssb-mentions

Extract the mentions in an SSB message, just using the markdown. This is _mostly compatible_ with the way Manyverse does it, but simpler, because it relies only on the markdown.

``` js
const mentions = require('remark-ssb-mentions')

const ary = mentions(markdown, opts)
```

This is basically the same as [ssb-mentions](https://github.com/ssbc/ssb-mentions), but implemented with [Unified](https://unifiedjs.com) instead of [ssb-marked](https://github.com/ssbc/ssb-marked).

## options

- `bareFeedNames` (boolean, default false): if true, include stub mention
  objects for bare feed name mentions, in the format `{name: "NAME", link: "@"}`. these can then have the `link` filled in with a feed id, to make a "patchwork-style mention", or be removed from the mentions array before publishing.

## License

MIT
